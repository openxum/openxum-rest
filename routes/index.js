exports = module.exports = function (app) {
  var express = require('express');
  var router = express.Router();

  var auth = require('./auth.js')(app);
  var user = require('./../controllers/users.js')(app);
  var game = require('./../controllers/games.js')(app);

  /*
   * Routes that can be accessed by any one
   */
  router.post('/login', auth.login);
  router.post('/create', user.create);

  router.get('/api/users', user.getAllUsers);
  router.delete('/api/user/:id/delete', user.delete);
  router.put('/api/user/:id/update', user.update);
  router.get('/api/user/:id/preferences', user.getUserPreferences);
  router.put('/api/user/:id/preferences', user.updateUserPreferences);
  router.put('/api/user/:id/active', user.active);
  router.put('/api/user/:id/inactive', user.inactive);

  /*
   * Routes that can be accessed only by authenticated users
   */

  router.get('/api/game/offline/:idUser', game.getOfflineGames);
  router.get('/api/game/offline/:idUser/waiting', game.getWaitingOfflineGames);
  router.get('/api/game/offline/:idGame/moves', game.getOfflineGameMoves);
  router.put('/api/game/offline/:idUser/:idGame/join', game.joinOfflineGame);
  router.put('/api/game/offline/:idGame/update', game.updateOfflineGame);
  router.get('/api/game/all/:idUser/:gameType', game.getAllGames);
  router.post('/api/game/create', game.createOfflineGame);
  router.delete('/api/game/:id/delete', game.delete);

  return router;
};