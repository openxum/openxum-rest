'use strict';

exports = module.exports = function (app) {

  var users = {
    active: function (req, res) {
      var idUser = req.params.id;

      app.db.models.User.update({_id: idUser}, {isActive: true}).then(function () {
        res.status(200).send(({
          "status": 200,
          "message": 'User is actived'
        }));
      }).catch(function (err) {
        res.status(400).send({
          "status": 400,
          "message": 'User isn\'t actived'
        })
      });
    },
    inactive: function (req, res) {
      var idUser = req.params.id;

      app.db.models.User.update({_id: idUser}, {isActive: false}).then(function () {
        res.status(200).send(({
          "status": 200,
          "message": 'User is inactived'
        }));
      }).catch(function (err) {
        res.status(400).send({
          "status": 400,
          "message": 'User isn\'t inactived'
        })
      });
    },
    // Return all users
    getAllUsers: function (req, res) {
      req.app.db.models.User.find({}, null, {safe: true}, function (err, users) {
        res.status(200).json(users);
      });
    },

    // Return user preferences
    getUserPreferences: function (req, res) {
      /* var idUser = req.params.id;
       app.db.models.User.findOne({
       where: {id: idUser},
       attributes: ['preferences']
       }).then(function (preferences) {
       res.status(200).json(preferences);
       }).catch(function (err) {
       res.status(400).send({
       "status": 400,
       "message": 'User not found'
       });
       }); */

      res.status(200).json({});
    },

    create: function (req, res) {
      req.app.db.models.User.schema.methods.encryptPassword(req.body.password, function (err, hash) {
        var user = {
          username: req.body.username,
          password: hash,
          email: req.body.email,
          isActive: true
        };

        req.app.db.models.User.create(user, function (err, user) {
          if (err) {
            res.status(400).send({
              "status": 400,
              "message": 'User not created'
            });
          }
          res.status(200).send({
            "status": 200,
            "message": 'User created',
            "user": user
          });
        });
      });
    },
    delete: function (req, res) {
      var user_id = req.params.id;

      req.app.db.models.User.findByIdAndRemove(user_id, function (err) {
        if (err) {
          res.status(400).send({
            "status": 400,
            "message": 'User not removed'
          });
        }
        res.status(200).send({
          "status": 200,
          "message": 'User removed'
        });
      });
    },
    update: function (req, res) {
      req.app.db.models.User.schema.methods.encryptPassword(req.body.password, function (err, hash) {
        var idUser = req.params.id;
        var user = {
          username: req.body.username,
          password: hash,
          email: req.body.email,
          isActive: req.body.isActive
        };

        app.db.models.User.update({_id: idUser}, user).then(function () {
          res.status(200).send(({
            "status": 200,
            "message": 'User is updated'
          }));
        }).catch(function (err) {
          res.status(400).send({
            "status": 400,
            "message": 'User isn\'t updated'
          })
        });
      });
    },
    // Update user preferences
    updateUserPreferences: function (req, res) {
      /* var idUser = req.params.id;

       app.db.models.User.update({
       preferences: req.body.preferences
       }, {
       where: {id: idUser},
       attributes: ['preferences'],
       truncate: true
       }).then(function () {
       res.status(200).send(({
       "status": 200,
       "message": 'User update'
       }));

       }).catch(function (err) {
       res.status(400).send({
       "status": 400,
       "message": 'User not update'
       })
       }); */

      res.status(400).send({
        "status": 400,
        "message": 'User not update'
      })

    }

  };

  return users;
};