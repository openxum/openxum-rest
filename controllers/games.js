'use strict';

exports = module.exports = function (app) {

  var games = {

    // Create offline game
    createOfflineGame: function (req, res) {
      req.app.db.models.GameType.findOne({name: req.body.gameType}, null,
        {safe: true}, function (err, gametype) {
          if (gametype) {
            req.app.db.models.Game.findOne({name: req.body.name}, null,
              {safe: true}, function (err, game) {
                if (!game) {
                  var fieldsToSet = {
                    name: req.body.name,
                    gameType: gametype._id,
                    color: req.body.color,
                    mode: req.body.mode,
                    type: 'offline',
                    status: 'wait',
                    userCreated: {
                      id: req.user.id,
                      name: req.user.username
                    },
                    opponent: {id: null},
                    currentColor: req.body.color
                  };
                  req.app.db.models.Game.create(fieldsToSet, function (err, user) {
                    res.status(200).json({});
                  });
                }
              });
          }
        });
    },
    delete: function (req, res) {
      var game_id = req.params.id;

      req.app.db.models.Game.findByIdAndRemove(game_id, function (err) {
        if (err) {
          res.status(400).send({
            "status": 400,
            "message": 'Game not removed'
          });
        }
        res.status(200).send({
          "status": 200,
          "message": 'Game removed'
        });
      });
    },
    // Return offline games of a user
    getOfflineGames: function (req, res) {
      req.app.db.models.Game.find({
          type: 'offline', $or: [
            {'userCreated.id': req.params.idUser},
            {'opponent.id': req.params.idUser}
          ]
        }, null,
        function (err, games) {
          if (games.length > 0) {
            var gamesdetail = [];
            var notdone = true;

            games.forEach(function (game) {
              req.app.db.models.GameType.findOne({_id: game.gameType}, null,
                {safe: true}, function (err, gameType) {
                  var game_type_name = gameType.name;

                  req.app.db.models.User.findOne({_id: game.userCreated.id}, null,
                    {safe: true}, function (err, userCreated) {
                      game.userCreated.name = userCreated.username;
                      if (game.opponent.id !== null) {
                        req.app.db.models.User.findOne({_id: game.opponent.id}, null,
                          {safe: true}, function (err, opponent) {
                            var opponent_name = null;

                            if (opponent !== null) {
                              opponent_name = opponent.username;
                            }
                            gamesdetail.push({
                              game: game,
                              opponent: opponent_name,
                              game_type_name: game_type_name
                            });
                            if (gamesdetail.length === games.length) {
                              if (notdone) {
                                res.status(200).json(gamesdetail);
                                notdone = false;
                              }
                            }
                          });
                      } else {
                        if (gamesdetail.length === games.length) {
                          if (notdone) {
                            res.status(200).json(gamesdetail);
                            notdone = false;
                          }
                        }
                      }
                    });
                });
            });
          } else {
            res.status(200).json([]);
          }
        });
    },
    getOfflineGameMoves: function (req, res) {
      var idGame = req.params.idGame;

      req.app.db.models.Game.findOne({_id: idGame}, null,
        function (err, game) {
          if (err) {
            res.status(400).send({
              "status": 400,
              "message": 'Game doesn\'t found'
            });
          } else {
            res.status(200).json(game.moves);
          }
        });
    },
    getWaitingOfflineGames: function (req, res) {
      req.app.db.models.Game.find({
          type: 'offline',
          status: 'wait',
          'userCreated.id': {$ne: req.params.idUser},
          'opponent.id': {$ne: req.params.idUser}
        }, null,
        function (err, games) {
          if (games && games.length > 0) {
            var gamesdetail = [];
            var notdone = true;

            games.forEach(function (game) {
              req.app.db.models.GameType.findOne({_id: game.gameType}, null,
                {safe: true}, function (err, gameType) {
                  game.type = gameType.name;
                  req.app.db.models.User.findOne({_id: game.userCreated.id}, null,
                    {safe: true}, function (err, userCreated) {
                      game.userCreated.name = userCreated.username;
                      gamesdetail.push(game);
                      if (gamesdetail.length === games.length) {
                        if (notdone) {
                          res.status(200).json(gamesdetail);
                          notdone = false;
                        }
                      }
                    });
                });
            });
          } else {
            res.status(200).json([]);
          }
        });
    },

    // Return all games
    getAllGames: function (req, res) {
      req.app.db.models.GameType.findOne({name: req.params.gameType}, null, {safe: true}, function (err, gameType) {
        req.app.db.models.Game.find({game: gameType._id, type: 'online', 'userCreated.id': req.params.idUser}, null,
          {safe: true}, function (err, games) {
            var my_online_games = games ? games : [];

            req.app.db.models.Game.find({
                game: gameType._id,
                type: 'offline',
                status: 'wait',
                'userCreated.id': req.params.idUser
              }, null,
              {safe: true}, function (err, games) {
                var my_offline_games = games ? games : [];

                req.app.db.models.Game.find({
                    game: gameType._id,
                    type: 'online',
                    'userCreated.id': {'$ne': req.params.idUser}
                  }, 'name color mode userCreated.name',
                  {safe: true}, function (err, games) {
                    var other_online_games = games ? games : [];

                    req.app.db.models.Game.find({
                        game: gameType._id,
                        type: 'offline',
                        status: 'wait',
                        'userCreated.id': {'$ne': req.params.idUser}
                      }, 'name color mode userCreated.name userCreated.id',
                      {safe: true}, function (err, games) {
                        var other_offline_games = games ? games : [];
                        res.status(200).json({
                          game: req.params.gameType,
                          user_id: req.params.idUser,
                          my_online_games: my_online_games,
                          my_offline_games: my_offline_games,
                          other_online_games: other_online_games,
                          other_offline_games: other_offline_games
                        });
                      });
                  });
              });
          });
      });
    },

    joinOfflineGame: function (req, res) {
      var idUser = req.params.idUser;
      var idGame = req.params.idGame;

      app.db.models.Game.update({_id: idGame}, {status: 'run', opponent: {id: idUser}}).then(function () {
        res.status(200).send(({
          "status": 200,
          "message": 'Game is joined'
        }));
      }).catch(function (err) {
        res.status(400).send({
          "status": 400,
          "message": 'Game isn\'t joined'
        })
      });
    },

    updateOfflineGame: function (req, res) {
      var idGame = req.params.idGame;

      app.db.models.Game.update({_id: idGame}, {
        moves: req.body.moves,
        currentColor: req.body.current_color
      }).then(function () {
        res.status(200).send(({
          "status": 200,
          "message": 'Game is updated'
        }));
      }).catch(function (err) {
        res.status(400).send({
          "status": 400,
          "message": 'Game isn\'t updated'
        })
      });
    }
  };

  return games;
};